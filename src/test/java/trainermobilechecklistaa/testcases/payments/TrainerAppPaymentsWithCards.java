package trainermobilechecklistaa.testcases.payments;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mobiletrainer.qa.module.actions.TraineAndroidMobileAppSessionPaymentAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLoginAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLogoutAction;
import com.mobiletrainer.qa.module.helpers.TrainerAndroidAppBaseClass;
import com.mobiletrainer.qa.module.testdata.TrainerAndroidAppDataProvider;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAppPaymentsWithCards {
    static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void TrainerAndroidMobileAppAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = TrainerAndroidAppBaseClass.LanuchMobileApp();	
	Log.info("Trainer Android Mobile App Launched");}
	
@Test (dataProvider = "TrainerAppPaymentsWithCards" , dataProviderClass = TrainerAndroidAppDataProvider.class)
public void TrainerMobileAppLogin(String Username ,String Password) throws Exception{
	TrainerAndroidMobileLoginAction.TrainerMobileAppLogin(driver,Username,Password);
	TraineAndroidMobileAppSessionPaymentAction.SessionPaymentwithCard(driver,"Workout");
//	TrainerAndroidMobileLogoutAction.TrainerAppLogOut(driver);
   Log.info("Successfully validated Trainer Login functionality with valid inputs");}

@AfterClass
public void TrainerMobileCloseApp() throws Exception{
//	driver.quit();
	Log.info("Trainer  Android App closed succesfully");}
}
