package trainermobilechecklistaa.testcases.logins;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileFacebookLoginAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLogoutAction;
import com.mobiletrainer.qa.module.helpers.TrainerAndroidAppBaseClass;
import com.mobiletrainer.qa.module.testdata.TrainerAndroidAppDataProvider;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class MobileAppFacebookLogin {

	static AndroidDriver<AndroidElement> driver;
	
@BeforeClass
public void TrainerMobileAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = TrainerAndroidAppBaseClass.LanuchMobileApp();	
	Log.info("Trainer Android Mobile App Launched");}
	
@Test (dataProvider = "TrainerAndroidAppFacebookLogin" , dataProviderClass = TrainerAndroidAppDataProvider.class)
public void TrainerMobileFacebookAppLogin(String Facebookid,String FacebookPswd) throws Exception{
	TrainerAndroidMobileFacebookLoginAction.TrainerMobileAppFacebookLogin(driver ,Facebookid ,FacebookPswd);
	//TrainerAndroidMobileLogoutAction.TrainerAppLogOut(driver);
    Log.info("Successfully validated Trainer App Facebook Login functionality with valid inputs");}

@AfterClass
public void TrainerMobileCloseApp() throws Exception{
	driver.quit();
	Log.info("Trainer Android App closed succesfully");}
}
