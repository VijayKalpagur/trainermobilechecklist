package testcases.logins;


import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLoginAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLogoutAction;
import com.mobiletrainer.qa.module.helpers.TrainerAndroidAppBaseClass;
import com.mobiletrainer.qa.module.testdata.TrainerAndroidAppDataProvider;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class MobileAppLogin {
	
     static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void TrainerAndroidMobileAppAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = TrainerAndroidAppBaseClass.LanuchMobileApp();	
	Log.info("Trainer Android Mobile App Launched");}
	
@Test (dataProvider = "TrainerAndroidAppLogin" , dataProviderClass = TrainerAndroidAppDataProvider.class)
public void TrainerMobileAppLogin(String Username ,String Password) throws Exception{
	TrainerAndroidMobileLoginAction.TrainerMobileAppLogin(driver,Username,Password);
	TrainerAndroidMobileLogoutAction.TrainerAppLogOut(driver);}

@AfterClass
public void TrainerMobileCloseApp() throws Exception{
	driver.quit();}
}
