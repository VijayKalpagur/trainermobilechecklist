package testcases.profile;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileAppProfileAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLoginAction;
import com.mobiletrainer.qa.module.helpers.TrainerAndroidAppBaseClass;
import com.mobiletrainer.qa.module.testdata.TrainerAndroidAppDataProvider;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class DeleteAccount {
	static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void TrainerAndroidMobileAppAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = TrainerAndroidAppBaseClass.LanuchMobileApp();	
	Log.info("Trainer Android Mobile App Launched");}
		
@Test (dataProvider = "TrainerAndroidAppLogin" , dataProviderClass = TrainerAndroidAppDataProvider.class)
public void TrainerAndroidAppDeleteAccount(String Username ,String Password) throws Exception{
	TrainerAndroidMobileLoginAction.TrainerMobileAppLogin(driver,Username,Password);
	TrainerAndroidMobileAppProfileAction.TrainerMobileAppDeleteAccount(driver);}

@AfterClass
public void TrainerMobileCloseApp() throws Exception{
	driver.quit();
	Log.info("Trainer  Android App closed succesfully");}
}
