package testcases.createsessions;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileAppCustomSessionAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLoginAction;
import com.mobiletrainer.qa.module.actions.TrainerAndroidMobileLogoutAction;
import com.mobiletrainer.qa.module.helpers.TrainerAndroidAppBaseClass;
import com.mobiletrainer.qa.module.testdata.TrainerAndroidAppDataProvider;
import com.mobiletrainer.qa.utility.Log;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerMobileAppCustomWorkoutSession {

	static AndroidDriver<AndroidElement> driver;

@BeforeClass
public void trainerMobileAppLaunch() throws Exception{
	PropertyConfigurator.configure("log4j.properties");	
	driver = TrainerAndroidAppBaseClass.LanuchMobileApp();	
	Log.info("Trainer Android Mobile App Launched");}
		   	
@Test (dataProvider = "CustomSessionsaccountLogin" , dataProviderClass = TrainerAndroidAppDataProvider.class)
public void trainerAppCustomWorkoutSession(String Username ,String Password) throws Exception{
	TrainerAndroidMobileLoginAction.TrainerMobileAppLogin(driver,Username,Password);
	TrainerAndroidMobileAppCustomSessionAction.TrainerAppCustomWorkoutSession(driver,"Workout Session");
	TrainerAndroidMobileLogoutAction.TrainerAppLogOut(driver);
	Log.info("Successfully created Custom Workout Session with valid inputs in Mobile Trainer App");}	

@AfterClass
public void trainerMobileCloseApp() throws Exception{
driver.quit();
Log.info("Trainer App closed succesfully");}
	}

