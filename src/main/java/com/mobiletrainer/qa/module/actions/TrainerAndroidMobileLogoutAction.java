package com.mobiletrainer.qa.module.actions;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppLoginPage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileLogoutAction {
	
public static void TrainerAppLogOut(AndroidDriver<AndroidElement> driver) throws Exception{	
	TrainerAndroidMobileAppLoginPage Login = new TrainerAndroidMobileAppLoginPage(driver);
	TrainerAndroidMobileAppLoginPage Logout = new TrainerAndroidMobileAppLoginPage(driver);
	
	Thread.sleep(1000);
	Login.TrainerAppprofiletab.click();
	Log.info("Click action performed on profile tab.");
	Thread.sleep(1000);
	
	//TrainerAndroidMobileActions.menuPageScrollup(driver);
	//Thread.sleep(2000);
	
	Logout.TrainerAppLogout.click();
	Log.info("Trainer App Logout Succesfully.");
	Thread.sleep(2000);
	Log.info("Trainer  Android App closed succesfully");}

}

