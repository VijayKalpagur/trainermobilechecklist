package com.mobiletrainer.qa.module.actions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppCreateWeeklySessionPage;
import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppCreateWeeklySessionPage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileAppWeeklySessionAction {

public static void TrainerAppWeeklyWorkoutSession(AndroidDriver<AndroidElement> driver,String SessionName) throws Exception{
	
TrainerAndroidMobileAppCreateWeeklySessionPage WeeklySession = new TrainerAndroidMobileAppCreateWeeklySessionPage(driver);
WebDriverWait wait = new WebDriverWait(driver, 100);	
	WeeklySession.TrainerAppMenuIcon.click();
	Log.info("Click action performed on Menu Icon.");

	WeeklySession.WorkoutSessionTab.click();
    Thread.sleep(2000);
	Log.info("Click action performed on WorkoutSessionTab.");

	wait.until(ExpectedConditions.elementToBeClickable(WeeklySession.WorkoutSessionsDraft)).click();
	Log.info("Click action performed on WorkoutSessionsDraft Tab.");
	Thread.sleep(1000);
	
	WeeklySession.ClickonWorkoutSession.click();
	Log.info("Click action performed on WorkoutSession is selected");	
	Thread.sleep(5000);
	
	WeeklySession.EditandPublishButton.click();
	Log.info("Click action performed on Edit and Publish Button");
	Thread.sleep(5000);
	
	TrainerAndroidMobileActions.menuPageScrollup(driver);
	Thread.sleep(1000);
	TrainerAndroidMobileActions.menuPageScrollup(driver);
	
//	WeeklySession.PublishButton.click();
	MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[14]/android.view.View/android.widget.Button");
	el1.click();
	Thread.sleep(15000);
	
	//WeeklySession.UpdateButton.click();
	MobileElement el2 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[13]/android.view.View/android.widget.Button");
	el2.click();
	Log.info("Click action performed on Update Button");
	Thread.sleep(4000);
//	
	WeeklySession.BackArrowIcon.click();
//	MobileElement el4 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View/android.widget.Button");
	//	el4.click();
	Thread.sleep(2000);
	
	WeeklySession.UpcomingSessionTab.click();
	Log.info("Click action performed on Upcoming Session Tab.");
	Thread.sleep(5000);
		
	WeeklySession.WorkoutSessionList.click();
	Log.info("Click action performed on Workout Session List.");
	Thread.sleep(2000);
	
	WeeklySession.DeleteWorkoutSession.click();
	Thread.sleep(2000);
	
	WeeklySession.ConfirmDeleteWorkoutSession.click();
	 Log.info("Workout Session Deleted Succesfully.");
	Thread.sleep(4000);	
}	
}
