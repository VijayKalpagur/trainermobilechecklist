package com.mobiletrainer.qa.module.actions;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppGmailPage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileGmailLoginAction {

public static void TrainerMobileAppGmailLogin(AndroidDriver<AndroidElement> driver , String Gmailid,String GmailPswd) throws Exception {

	TrainerAndroidMobileAppGmailPage GmailLogin = new TrainerAndroidMobileAppGmailPage(driver);
				
	GmailLogin.ConnectWithGoogleBtn.click();
	Log.info("Click action performed on Connect With Google Button.");
	Thread.sleep(3000);
	
	GmailLogin.GmailAccountUserEmailID.sendKeys(Gmailid);
	Log.info("Entered Gmail valid Email ID");
	Thread.sleep(2000);
	
	//GmailLogin.GmailAccountUserEmailPageStaticSignintext.click();
	
	GmailLogin.GmailEmailIDNextBtn.click();
	Log.info("Click action performed on Gmail Next Button.");
	Thread.sleep(2000);
	
	GmailLogin.GmailAccountEmailPassword.sendKeys(GmailPswd);
	Log.info("Entered Gmail valid Password");
	Thread.sleep(2000);
	
	//GmailLogin.GmailAccountUserPasswordPageStaticWelcometext.click();
	
	GmailLogin.GmailPaswordNextBtn.click();	
	Log.info("Click action performed on Gmail Next Button.");
	//GmailLogin.ExsitingMailAccountBtn.click();
	Thread.sleep(10000);
	
	GmailLogin.GmailLoginDashboardVerification.isDisplayed();
	Log.info("Trainer Mobile App Dashboard Verified.");
	Thread.sleep(1000);}
}


