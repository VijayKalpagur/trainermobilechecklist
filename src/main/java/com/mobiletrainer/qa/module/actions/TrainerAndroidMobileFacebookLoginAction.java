package com.mobiletrainer.qa.module.actions;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppFacebookPage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileFacebookLoginAction {

public static void TrainerMobileAppFacebookLogin(AndroidDriver<AndroidElement> driver , String Facebookid,String FacebookPswd) throws Exception {

	TrainerAndroidMobileAppFacebookPage FBLogin = new TrainerAndroidMobileAppFacebookPage(driver);
			
	FBLogin.ConnectWithFacebookBtn.click();
	Log.info("Click action performed on Connect With Facebook Button.");
	Thread.sleep(3000);
	
	FBLogin.FacebookAccountUserEmailID.sendKeys(Facebookid);
	Log.info("Entered Facebook valid Email ID");
	Thread.sleep(1000);
	
	FBLogin.FacebookAccountPassword.sendKeys(FacebookPswd);
	Log.info("Entered Facebook valid Password");
	
	FBLogin.FacebookAccountStactictext.click();
	
	FBLogin.FacebookLoginBtn.click();
	Log.info("Click action performed on Facebook Login Button.");
	Thread.sleep(3000);
	
	FBLogin.FacebookContinueBtn.click();
	Log.info("Click action performed on Facebook Continue Button.");
	Thread.sleep(000);
	
	FBLogin.FacebookLoginDashboardVerification.isDisplayed();
	Log.info("Trainer Mobile App Dashboard Verified.");
	Thread.sleep(1000);}
}


