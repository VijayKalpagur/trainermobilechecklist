package com.mobiletrainer.qa.module.actions;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppLoginPage;
import com.mobiletrainer.qa.utility.Log;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileLoginAction {

public static void TrainerMobileAppLogin(AndroidDriver<AndroidElement> driver , String Uname ,String Pswd) throws Exception {

	TrainerAndroidMobileAppLoginPage Login = new TrainerAndroidMobileAppLoginPage(driver);
	
	Thread.sleep(3000);
	
	Login.TrainerApplandingpageskipbutton.click();
	Thread.sleep(2000);
	
	Login.TrainerAppLoginLink.click();

	Login.TrainerAppUserEmailID.sendKeys(Uname);
	Log.info("Entered Trainer valid username");
	
	Thread.sleep(1000);

	Login.TrainerAppUserEmailPassword.sendKeys(Pswd);
	Log.info("Entered Trainer valid Password");

	Thread.sleep(1000);
	
	Login.TrainerAppLoginBtn.click();
	Log.info("Click action performed on Loginbutton.");
	Thread.sleep(5000);
	
	//Login.TrainerAppLoginDashboardVerification.isDisplayed();
	//Log.info("Trainer Mobile App Dashboard Verified.");
	Thread.sleep(1000);
	Log.info("Successfully validated Trainer App Login functionality with valid inputs");
	}
}
