package com.mobiletrainer.qa.module.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mobiletrainer.qa.module.pages.TraineAndroidMobileAppSessionsPaymentPages;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TraineAndroidMobileAppSessionPaymentAction {
	
	public static void SessionPaymentwithCard(AndroidDriver<AndroidElement> driver,String SessionName) throws Exception{
		TraineAndroidMobileAppSessionsPaymentPages cardpayment = new TraineAndroidMobileAppSessionsPaymentPages(driver);    
		
		 WebDriverWait wait = new WebDriverWait(driver, 100);	
		 cardpayment.TrainerAppMenuIcon.click();
			Log.info("Click action performed on Menu Icon.");
		
			cardpayment.WorkoutSessionTab.click();
		    Thread.sleep(2000);
			Log.info("Click action performed on WorkoutSessionTab.");
		
			wait.until(ExpectedConditions.elementToBeClickable(cardpayment.WorkoutSessionsDraft)).click();
			Log.info("Click action performed on WorkoutSessionsDraft Tab.");
			Thread.sleep(1000);
			
			cardpayment.ClickonWorkoutSession.click();
			Log.info("Click action performed on WorkoutSession is selected");	
			Thread.sleep(5000);
			
			cardpayment.EditandPublishButton.click();
			Log.info("Click action performed on Edit and Publish Button");
			Thread.sleep(5000);
			
			TrainerAndroidMobileActions.menuPageScrollup(driver);
			Thread.sleep(1000);
			TrainerAndroidMobileActions.menuPageScrollup(driver);
			
			TrainerAndroidMobileActions.menuPageScrollup(driver);
			Thread.sleep(1000);
			TrainerAndroidMobileActions.menuPageScrollup(driver);
			Thread.sleep(2000);
			
			cardpayment.SessionPrice.click();
		
			cardpayment.SessionPrice.clear();
			
			cardpayment.SessionPrice.sendKeys("00");
			Thread.sleep(2000);

		    cardpayment.CliconStaticYourRevenueText.click();
			Thread.sleep(1000);
				
			driver.findElement(By.xpath("")).click();
			cardpayment.PayPublishButton.click();
			Log.info("Click action performed on Publish Button.");
			Thread.sleep(3000);
			
			cardpayment.CreditAndDebitCardTab.click();
			
			cardpayment.CardNumberField.sendKeys("4242424242424242");
			Thread.sleep(1000);
			
			cardpayment.ClickonStaticCardNumberText.click();
			
			cardpayment.ClickonExpiration.click();
			Thread.sleep(1000);

			cardpayment.SelectCardExpireYear.click();
			Thread.sleep(2000);
			
			cardpayment.SelectCardExpireYearDoneButton.click();
			
			cardpayment.ClickonCVC.click();
			
			cardpayment.ClickonCVC.sendKeys("123");
			
			cardpayment.ClickonCVCStaticText.click();
			
			cardpayment.ClickonPayButton.click();
			
			TrainerAndroidMobileActions.menuPageScrollup(driver);
			Thread.sleep(1000);
			
			cardpayment.ClickonBacktoWorkoutSessionButton.click();
		//	cardpayment.PublishButton.click();
			MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[14]/android.view.View/android.widget.Button");
			el1.click();
			Thread.sleep(10000);
			
			//cardpayment.UpdateButton.click();
			MobileElement el2 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[13]/android.view.View/android.widget.Button");
			el2.click();
			Log.info("Click action performed on Update Button");
			Thread.sleep(4000);
//			
			cardpayment.BackArrowIcon.click();
//			MobileElement el4 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View/android.widget.Button");
			//	el4.click();
			Thread.sleep(1000);
			
			cardpayment.UpcomingSessionTab.click();
			Log.info("Click action performed on Upcoming Session Tab.");
			Thread.sleep(5000);
				
			cardpayment.WorkoutSessionList.click();
			Log.info("Click action performed on Workout Session List.");
			Thread.sleep(2000);
			
			cardpayment.DeleteWorkoutSession.click();
			Thread.sleep(2000);
			
			cardpayment.ConfirmDeleteWorkoutSession.click();
			 Log.info("Workout Session Deleted Succesfully.");
			Thread.sleep(4000);	
		
//		singlesession.TrainingLevelSelection.isDisplayed();
//		Log.info("Session TrainingLevel is selected");
		TrainerAndroidMobileActions.menuPageScrollup(driver);
		Thread.sleep(1000);
		TrainerAndroidMobileActions.menuPageScrollup(driver);
		Thread.sleep(2000);
		
		cardpayment.SessionPrice.click();
	
		cardpayment.SessionPrice.clear();
		
		cardpayment.SessionPrice.sendKeys("00");
		Thread.sleep(2000);

	    cardpayment.CliconStaticYourRevenueText.click();
		Thread.sleep(1000);
			
		driver.findElement(By.xpath("")).click();
		cardpayment.PayPublishButton.click();
		Log.info("Click action performed on Publish Button.");
		Thread.sleep(3000);
		
		cardpayment.CreditAndDebitCardTab.click();
		
		cardpayment.CardNumberField.sendKeys("4242424242424242");
		Thread.sleep(1000);
		
		cardpayment.ClickonStaticCardNumberText.click();
		
		cardpayment.ClickonExpiration.click();
		Thread.sleep(1000);

		cardpayment.SelectCardExpireYear.click();
		Thread.sleep(2000);
		
		cardpayment.SelectCardExpireYearDoneButton.click();
		
		cardpayment.ClickonCVC.click();
		
		cardpayment.ClickonCVC.sendKeys("123");
		
		cardpayment.ClickonCVCStaticText.click();
		
		cardpayment.ClickonPayButton.click();
		
		TrainerAndroidMobileActions.menuPageScrollup(driver);
		Thread.sleep(1000);
		
		cardpayment.ClickonBacktoWorkoutSessionButton.click();
		
		cardpayment.SessionPrice.click();
		
		cardpayment.ClickonDeleteButton.click();
		
		cardpayment.ClickonYesButton.click();
		
		cardpayment.RefundTextConfirmationText.click();
		Thread.sleep(4000);		
			
		}	

//	public static void PaymentwithPayPal(WebDriver driver) throws Exception{
//		CreateSingleSessionPage singlesession = new CreateSingleSessionPage(driver);
//		SessionsPaymentPages SessionPayment = new SessionsPaymentPages(driver);
//		
//		singlesession.TrainerAppMenuIcon.click();
//		Log.info("Click action performed on Menu Icon.");
//	
//		singlesession.CreateScheduleButton.click();
//	    Thread.sleep(2000);
//		Log.info("Click action performed on CreateScheduleButton.");
//		
//		singlesession.NameoftheWorkoutSession.sendKeys(SessionName);
//		Log.info("Entered Workout Session Name.");
//		
//		Thread.sleep(2000);
//		singlesession.ClickonCreateScehduleStaticText.click();
//				
//		singlesession.TypeofWorkoutSession.isDisplayed();
//		Log.info("Session TypeofWorkout is selected");
//			
//		singlesession.WorkoutActivity.click();
//		Log.info("Click action performed on WorkoutActivity.");
//	
//		singlesession.WorkoutAcvtivitySelection.click();
//		Thread.sleep(2000);
//		
//		singlesession.ClickonOkTickmarkforselection.click();
//		Log.info("Session WorkoutActivity is selected");
//				
//		singlesession.AddSessionDateandTimeButton.click();
//		Log.info("Click action performed on Add Session Date and Time Button");
//		Thread.sleep(2000);
//		
//		singlesession.ClickonAddSessionDate.click();
//		singlesession.ClickonSessionOnDate.click();
//		singlesession.ClickonDateselectionOkButton.click();
//		Log.info("Session Date is selected");
//		Thread.sleep(2000);
//		
//		singlesession.ClickonStartsFrom.click();
//		singlesession.SessionStartsFromTime.click();
//		singlesession.ClickonTimeSectionokButton.click();
//		Log.info("Session time is selected");
//		
//		singlesession.SessionDuration.isDisplayed();
////		Log.info("Session Duration is selected");
////		singlesession.SelectSessionDurationMinutes.click();
//		
//		singlesession.ClickonAddButton.click();
//		Log.info("Click action performed on Add Button.");
//		Thread.sleep(1000);
//		
//		TrainerAndroidMobileActions.menuPageScrollup(driver);
//		Thread.sleep(1000);
//		TrainerAndroidMobileActions.menuPageScrollup(driver);
//		
////		singlesession.TrainingLevelSelection.isDisplayed();
////		Log.info("Session TrainingLevel is selected");
//		
//		singlesession.PublishButton.click();
//		Log.info("Click action performed on Publish Button.");
////Publishbutton alternate Xpath
////		MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[13]/android.view.View/android.widget.Button");
////		el1.click();
////	    driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[13]/android.view.View/android.widget.Button\r\n" + "")).click();
//		
//	    Thread.sleep(3000);
//		//Log.info("Click action performed on Publish Button.");
//		
//		singlesession.DeleteWorkoutSession.click();
//		singlesession.ConfirmDeleteWorkoutSession.click();
//		 Log.info("Workout Session Deleted Succesfully.");
//		Thread.sleep(4000);	
//			
//			}
}
