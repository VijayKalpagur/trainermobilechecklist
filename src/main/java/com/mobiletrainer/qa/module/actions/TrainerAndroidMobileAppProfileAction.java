package com.mobiletrainer.qa.module.actions;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppProfilePage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileAppProfileAction {
	
public static void TrainerMobileAppDeleteAccount(AndroidDriver<AndroidElement> driver) throws Exception {

		TrainerAndroidMobileAppProfilePage Delete = new TrainerAndroidMobileAppProfilePage(driver);
		
		Delete.TrainerAppprofiletab.click();
		Thread.sleep(1000);
		
		Delete.TrainerAppDeleteAccount.click();
		Thread.sleep(1000);
		
		Delete.TrainerAppEnterDELETEText.sendKeys("DELETE");
		Thread.sleep(1000);
		
		Delete.TrainerAppClickonDeleteButton.click();
		Thread.sleep(1000);
		
		//Login.TrainerAppLoginDashboardVerification.isDisplayed();
		//Log.info("Trainer Mobile App Dashboard Verified.");
		Thread.sleep(1000);
		Log.info("Successfully validated Trainer App Delete functionality with valid inputs");
		}
}
