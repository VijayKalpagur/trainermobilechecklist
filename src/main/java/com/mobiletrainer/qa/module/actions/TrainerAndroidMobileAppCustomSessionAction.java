package com.mobiletrainer.qa.module.actions;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppCreateCustomSessionPage;
import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppCreateCustomSessionPage;
import com.mobiletrainer.qa.utility.Log;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileAppCustomSessionAction {

public static void TrainerAppCustomWorkoutSession(AndroidDriver<AndroidElement> driver,String SessionName) throws Exception{
	
	TrainerAndroidMobileAppCreateCustomSessionPage CustomSession = new TrainerAndroidMobileAppCreateCustomSessionPage(driver);
    WebDriverWait wait = new WebDriverWait(driver, 100);	
		CustomSession.TrainerAppMenuIcon.click();
		Log.info("Click action performed on Menu Icon.");
	
		CustomSession.WorkoutSessionTab.click();
	    Thread.sleep(2000);
		Log.info("Click action performed on WorkoutSessionTab.");
	
		wait.until(ExpectedConditions.elementToBeClickable(CustomSession.WorkoutSessionsDraft)).click();
		Log.info("Click action performed on WorkoutSessionsDraft Tab.");
		Thread.sleep(1000);
		
		CustomSession.ClickonWorkoutSession.click();
		Log.info("Click action performed on WorkoutSession is selected");	
		Thread.sleep(5000);
		
		CustomSession.EditandPublishButton.click();
		Log.info("Click action performed on Edit and Publish Button");
		Thread.sleep(5000);
		
		TrainerAndroidMobileActions.menuPageScrollup(driver);
		Thread.sleep(1000);
		TrainerAndroidMobileActions.menuPageScrollup(driver);
		
	//	CustomSession.PublishButton.click();
		MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[14]/android.view.View/android.widget.Button");
		el1.click();
		Thread.sleep(13000);
		
		//CustomSession.UpdateButton.click();
		MobileElement el2 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[13]/android.view.View/android.widget.Button");
		el2.click();
		Log.info("Click action performed on Update Button");
		Thread.sleep(4000);
//		
		CustomSession.BackArrowIcon.click();
//		MobileElement el4 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View/android.widget.Button");
		//	el4.click();
		Thread.sleep(2000);
		
		CustomSession.UpcomingSessionTab.click();
		Log.info("Click action performed on Upcoming Session Tab.");
		Thread.sleep(5000);
			
		CustomSession.WorkoutSessionList.click();
		Log.info("Click action performed on Workout Session List.");
		Thread.sleep(2000);
		
		CustomSession.DeleteWorkoutSession.click();
		Thread.sleep(2000);
		
		CustomSession.ConfirmDeleteWorkoutSession.click();
		 Log.info("Workout Session Deleted Succesfully.");
		Thread.sleep(4000);	
	}
}
