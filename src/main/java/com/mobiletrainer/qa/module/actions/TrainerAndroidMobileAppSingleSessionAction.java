package com.mobiletrainer.qa.module.actions;


import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mobiletrainer.qa.module.pages.TrainerAndroidMobileAppCreateSingleSessionPage;
import com.mobiletrainer.qa.utility.Log;



import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidMobileAppSingleSessionAction {
	
public static void TrainerAppSingleWorkoutSession(AndroidDriver<AndroidElement> driver,String SessionName) throws Exception{
	
	TrainerAndroidMobileAppCreateSingleSessionPage singlesession = new TrainerAndroidMobileAppCreateSingleSessionPage(driver);
    WebDriverWait wait = new WebDriverWait(driver, 100);	
		singlesession.TrainerAppMenuIcon.click();
		Log.info("Click action performed on Menu Icon.");
	
		singlesession.CreateScheduleButton.click();
	    Thread.sleep(3000);
		Log.info("Click action performed on CreateScheduleButton.");
		
		singlesession.NameoftheWorkoutSession.sendKeys("Session");
	    Thread.sleep(2000);
		Log.info("Entered name of the workoutsession");
				
		singlesession.WorkoutActivity.click();
		
		singlesession.WorkoutAcvtivitySelection.click();
		Thread.sleep(3000);
		
		singlesession.ClickonOkTickmarkforselection.click();
	    Thread.sleep(3000);
		Log.info("Session WorkoutActivity is selected");
		
//		singlesession.TrainingLevel.isDisplayed();
//		singlesession.TrainingLevel.click();
//		singlesession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
//	    Thread.sleep(1000);
//		Log.info("Session TrainingLevel is selected");
//		
		singlesession.AddSessionDateandTimeButton.click();
		singlesession.ClickonAddSessionDate.click();
		singlesession.ClickonSessionOnDate.click();
		singlesession.ClickonDateselectionOkButton.click();
		Thread.sleep(2000);
		Log.info("Session Date is selected");
		
		
		singlesession.ClickonStartsFrom.click();
		singlesession.SessionStartsFromTime.click();
		singlesession.ClickonTimeSectionokButton.click();
		Log.info("Session time is selected");
		Thread.sleep(1000);
		singlesession.ClickonAddButton.click();
		Thread.sleep(3000);
		
//		singlesession.SessionDuration.click();
//		singlesession.SessionStartsFromTime.click();
//		singlesession.ClickonTimeSectionokButton.click();
//		Log.info("Session Duration is selected");
	
//		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
//		
//		singlesession.PublishButton.isDisplayed();
//		singlesession.PublishButton.isEnabled();
		singlesession.PublishButton.click();
		Thread.sleep(18000);
		Log.info("Click action performed on Publish Button.");
//		
		singlesession.DeleteWorkoutSession.click();
		Thread.sleep(3000);
//		
		singlesession.ConfirmDeleteWorkoutSession.click();
		Log.info("Single Session Deleted Succesfully");
//		
		Thread.sleep(10000);
//
		Assert.assertEquals("No workout sessions found." ,"No workout sessions found.");
		Log.info("Successfully verified No workout sessions found.Text.");	

	}	
}


