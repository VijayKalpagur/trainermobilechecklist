package com.mobiletrainer.qa.module.testdata;

import org.testng.annotations.DataProvider;

public class TrainerAndroidAppDataProvider {

	@DataProvider(name = "TrainerAndroidAppLogin" )//New Registered User
	public static Object[][] getDataFromUserLoginDataprovider(){
	return new Object[][]{{"sunilkumar100@mailinator.com", "password"}};}
	
	@DataProvider(name = "TrainerAppPaymentsWithCards" )//New Registered User
	public static Object[][] getDataFromUserPaymentsDataprovider(){
	return new Object[][]{{"sunilkumar10@mailinator.com", "password"}};}
	
	@DataProvider(name = "TrainerAndroidAppGmailLogin" )//Social Login
	public static Object[][] gmailDataprovider(){
	return new Object[][]{{"sunilkumartestingfitbase@gmail.com", "Testing@123"}};}
	
	@DataProvider(name = "TrainerAndroidAppFacebookLogin" )//Social Login
	public static Object[][] FacebookDataprovider(){
	return new Object[][]{{"fitbase.mail1@gmail.com", "testing123$"}};}

	@DataProvider(name = "TrainerAndroidAppForgotPassword" )//Social Login
	public static Object[][] ForgotPasswordDataprovider(){
	return new Object[][]{{"sunilkumar@mailinator.com", "password"}};}
	
	@DataProvider(name = "SingleSessionsaccountLogin" )//New Registered User
	public static Object[][] SingleSessionsaccountDataProvider(){
	return new Object[][]{{"sunilkumar10@mailinator.com", "password"}};}	

	@DataProvider(name = "DailySessionsaccountLogin" )//New Registered User
	public static Object[][] DailySessionsaccountDataProvider(){
	return new Object[][]{{"sunilkumar301@mailinator.com", "password"}};}

	@DataProvider(name = "WeeklySessionsaccountLogin" )//New Registered User
	public static Object[][] WeeklySessionsaccountDataProvider(){
	return new Object[][]{{"sunilkumar302@mailinator.com", "password"}};}

	@DataProvider(name = "MonthlySessionsaccountLogin" )//New Registered User
	public static Object[][] MonthlySessionsaccountDataProvider(){
	return new Object[][]{{"sunilkumar303@mailinator.com", "password"}};}

	@DataProvider(name = "CustomSessionsaccountLogin" )//New Registered User
	public static Object[][] CustomSessionsaccountDataProvider(){
	return new Object[][]{{"sunilkumar304@mailinator.com", "password"}};}
}
