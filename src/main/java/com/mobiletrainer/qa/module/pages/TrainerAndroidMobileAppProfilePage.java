package com.mobiletrainer.qa.module.pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TrainerAndroidMobileAppProfilePage {
	
	static AndroidDriver<AndroidElement> driver;
	
public TrainerAndroidMobileAppProfilePage(AndroidDriver<AndroidElement> driver){
		
	PageFactory.initElements(new AppiumFieldDecorator(driver),this);}

	//---------------------------------* Delete Account *----------------------------------------

@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[8]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View\r\n" + "")
public AndroidElement TrainerAppDeleteAccount;

@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.app.Dialog/android.view.View[2]/android.view.View[4]/android.view.View/android.widget.EditText\r\n" + "")
public AndroidElement TrainerAppEnterDELETEText;

@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.app.Dialog/android.view.View[2]/android.widget.Button[2]\r\n" + "")
public AndroidElement TrainerAppClickonDeleteButton;

@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[5]/android.widget.Button\r\n" + "")
public AndroidElement TrainerAppprofiletab;
}
