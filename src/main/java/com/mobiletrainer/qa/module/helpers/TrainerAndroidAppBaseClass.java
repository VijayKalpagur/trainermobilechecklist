package com.mobiletrainer.qa.module.helpers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TrainerAndroidAppBaseClass {

	static AndroidDriver<AndroidElement> driver;
	
public static AndroidDriver<AndroidElement> LanuchMobileApp() throws MalformedURLException, Exception {
	
	try{
		// Created object of DesiredCapabilities class.
	  DesiredCapabilities capabilities = new DesiredCapabilities();

	  // Set android deviceName desired capability. Set it Android Emulator.
	  capabilities.setCapability("deviceName", "192.168.207.101:5555");

	  // Set browserName desired capability. It's Android in our case here.
	  // capabilities.setCapability("browserName", "Android");

	  // Set android platformVersion desired capability. Set your emulator's android version.
	   capabilities.setCapability("platformVersion", "10");

	  // Set android platformName desired capability. It's Android in our case here.
	  capabilities.setCapability("platformName", "Android");
	  
	  // Set your application's appPackage if you are using any other app.
	  capabilities.setCapability("appPackage", "com.fitbasetrainer");

	  // Set your application's appPackage if you are using any other app.
	  capabilities.setCapability("appActivity", "com.fitbasetrainer.MainActivity");

	  // Created object of RemoteWebDriver will all set capabilities.
	  // Set appium server address and port number in URL string.
	  // It will launch fitbaseTariner Native app in emulator.
	  driver = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  Thread.sleep(5000);
	  
	}catch(Exception e){
		System.out.println("Message is : " +e.getMessage());
		e.printStackTrace();
	}
	  return driver;	}
}